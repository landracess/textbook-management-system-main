# 教材管理系统


## 一、介绍

基于spring+spring mvc+mybatis+layui+jquery+bootstrap的教材管理系统

## 二、系统运行界面

![img_1.png](imgs/img_1.png)

## 三、系统功能截图

### 1、管理员

![img_2.png](imgs/img_2.png)

![img_3.png](imgs/img_3.png)

![img_4.png](imgs/img_4.png)

![img_5.png](imgs/img_5.png)

![img_6.png](imgs/img_6.png)

![img_7.png](imgs/img_7.png)

### 2、教师

![img_8.png](imgs/img_8.png)

![img_9.png](imgs/img_9.png)

### 3、学生

![img_10.png](imgs/img_10.png)

![img_11.png](imgs/img_11.png)

![img_12.png](imgs/img_12.png)


